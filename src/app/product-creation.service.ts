import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class ProductCreationService {


  uri = 'http://localhost:4000/product';
  constructor(private http: HttpClient) { }

    addProduct(product_id,product_name, product_description, product_manufacturer) {
      const obj = {
        product_id:product_id,
    product_name:product_name,
    product_description:product_description,
    product_manufacturer:product_manufacturer
      };
      console.log(obj);
      this.http.post(`${this.uri}/create`, obj)
          .subscribe(res => console.log('Done'));
    
  }
}
