import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './home/home.component';
import { from } from 'rxjs';
import { FarmerComponent } from './farmer/farmer.component';
import { AgencyComponent } from './agency/agency.component';
import { CustomerComponent } from './customer/customer.component';
import { FarmerSaleComponent } from './farmer-sale/farmer-sale.component';
import { AgencyEmployeeListComponent } from './agency-employee-list/agency-employee-list.component';
import { AgencyEmployeeRegistrationComponent } from './agency-employee-registration/agency-employee-registration.component';
import { CustomerPurchaseComponent } from './customer-purchase/customer-purchase.component';
import { ShopComponent } from './shop/shop.component';
import { ShopEmployeeListComponent } from './shop-employee-list/shop-employee-list.component';
import { ShopEmployeeRegistrationComponent } from './shop-employee-registration/shop-employee-registration.component';
import { ShopPurchaseComponent } from './shop-purchase/shop-purchase.component';
import { ShopSaleComponent } from './shop-sale/shop-sale.component';
import { ShopBalanceItemsComponent } from './shop-balance-items/shop-balance-items.component';

const routes: Routes = [
  {
    path:'',component:HomeComponent
  },
  {
    path:'farmer',component:FarmerComponent
  },
  {
    path:'product-creation',component:FarmerSaleComponent
  },
  {
    path:'agency',component:AgencyComponent
  },
  {
    path:'agency-employee-list',component:AgencyEmployeeListComponent
  },
  {
    path:'agency-employee-registration',component:AgencyEmployeeRegistrationComponent
  },
  {
    path:'customer',component:CustomerComponent
  },
  {
    path:'customer-purchase',component:CustomerPurchaseComponent
  },
  {
    path:'shop',component:ShopComponent
  },
  {
    path:'shop-employee-registration',component:ShopEmployeeRegistrationComponent
  },
  {
    path:'shop-employee-list',component:ShopEmployeeListComponent
  },
  {
    path:'shop-purchase',component:ShopPurchaseComponent
  },
  {
    path:'shop-sale',component:ShopSaleComponent
  },
  {
    path:'shop-balance-items',component:ShopBalanceItemsComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
