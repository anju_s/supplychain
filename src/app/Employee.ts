export default class Employee {
  emp_id:Number;
  emp_name: String;
  emp_address: String;
  emp_department: Number;
}
