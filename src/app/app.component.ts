import { Component } from '@angular/core';
import { NavigationCancel,
  Event,
  NavigationEnd,
  NavigationError,
  NavigationStart,Router } from "@angular/router";
import {SlimLoadingBarService} from 'ng2-slim-loading-bar';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ProductTracking inSupply Chain';
  constructor(private _loadingBar: SlimLoadingBarService,private router: Router) { 
    this.router.events.subscribe((event: Event) => {
      this.navigationInterceptor(event);
    });
  }
  LoadPage(page)
  {
    
    this.router.navigate([page]);console.log("Click!", event);
  }

  private navigationInterceptor(event: Event): void {
    if (event instanceof NavigationStart) {
      this._loadingBar.start();
    }
    if (event instanceof NavigationEnd) {
      this._loadingBar.complete();
    }
    if (event instanceof NavigationCancel) {
      this._loadingBar.stop();
    }
    if (event instanceof NavigationError) {
      this._loadingBar.stop();
    }
  }
}
