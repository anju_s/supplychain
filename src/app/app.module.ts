import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {MatMenuModule, MatButtonModule} from '@angular/material';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AgencyComponent } from './agency/agency.component';
import { ShopComponent } from './shop/shop.component';
import { FarmerComponent } from './farmer/farmer.component';
import { CustomerComponent } from './customer/customer.component';
import { FarmerSaleComponent } from './farmer-sale/farmer-sale.component';
import { AgencyEmployeeListComponent } from './agency-employee-list/agency-employee-list.component';
import { AgencyEmployeeRegistrationComponent } from './agency-employee-registration/agency-employee-registration.component';
import { CustomerPurchaseComponent } from './customer-purchase/customer-purchase.component';
import { ShopEmployeeListComponent } from './shop-employee-list/shop-employee-list.component';
import { ShopEmployeeRegistrationComponent } from './shop-employee-registration/shop-employee-registration.component';
import { ShopPurchaseComponent } from './shop-purchase/shop-purchase.component';
import { ShopSaleComponent } from './shop-sale/shop-sale.component';
import { ShopBalanceItemsComponent } from './shop-balance-items/shop-balance-items.component';
import { SlimLoadingBarModule } from 'ng2-slim-loading-bar';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AgencyComponent,
    ShopComponent,
    FarmerComponent,
    CustomerComponent,
    FarmerSaleComponent,
    AgencyEmployeeListComponent,
    AgencyEmployeeRegistrationComponent,
    CustomerPurchaseComponent,
    ShopEmployeeListComponent,
    ShopEmployeeRegistrationComponent,
    ShopPurchaseComponent,
    ShopSaleComponent,
    ShopBalanceItemsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatMenuModule, MatButtonModule, BrowserAnimationsModule,
    SlimLoadingBarModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

