import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import{ProductCreationService} from "../product-creation.service";

@Component({
  selector: 'app-farmer-sale',
  templateUrl: './farmer-sale.component.html',
  styleUrls: ['./farmer-sale.component.css']
})
export class FarmerSaleComponent implements OnInit {

  angForm: FormGroup;
  constructor(private fb: FormBuilder, private ps: ProductCreationService) {
    this.createForm();
  }

  createForm() {
    this.angForm = this.fb.group({
      product_id: ['', Validators.required ],
      product_name: ['', Validators.required ],
      product_description: ['', Validators.required ],
      product_manufacturer : ['', Validators.required ]
    });
  }

  addProducts(product_id,product_name,product_description, product_manufacturer) {
    this.ps.addProduct(product_id,product_name,product_description, product_manufacturer);
  }
  ngOnInit() {
  }

}
