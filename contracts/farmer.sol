pragma solidity ^0.5.0;

contract Farmer {
    address public farmerAddress;
    
    constructor() public {
        farmerAddress = msg.sender;
    }
    
    modifier onlyFarmer() {
        require (msg.sender==farmerAddress);
        _;
    }
}
