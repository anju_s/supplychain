pragma solidity ^0.5.0;

import "./farmer.sol";

contract AssetTracker is Farmer{
    string id;
    address ownerAddress;
    
    struct Asset {
        string name;
        string description;
        string manufacturer;
        bool initialized;
    }
    
    address[] addressArray;
    uint public ownerCount = 0;

    mapping(string => Asset) private AssetStore;
    mapping(address => mapping(string => bool)) private WalletStore;
    //mapping(string => Packaging) private PackageStore;
    
    event AssetCreate(address account, string uuid, string manufacturer);
	event RejectCreate(address account, string uuid, string message);
	event AssetTransfer(address from, address to, string uuid);
	//event RejectTransfer(address from, address to, string uuid, string message);
	/*event PackageCreate(address account, string uuid, string agency);
	event RejectPackage(address account, string uuid, string message);*/
	
	function createAsset(string memory _name, string memory _description, string memory _uuid, string memory _manufacturer) public onlyFarmer {
		//if(AssetStore[_uuid].initialized) {
		//	emit RejectCreate(msg.sender, _uuid, "Asset with this UUID already exists.");
		//	return;
		//}
		require((!AssetStore[_uuid].initialized),"Asset with this UUID already exists.");
		
		ownerAddress = msg.sender;
		addressArray.push(msg.sender);
		ownerCount++;
		AssetStore[_uuid] = Asset(_name, _description, _manufacturer, true);
		WalletStore[msg.sender][_uuid] = true;
		emit AssetCreate(msg.sender, _uuid, _manufacturer);
	}
		function displayAsset(string memory _uuid) public view   returns (string memory _name, string memory _description, string memory _manufacturer) {
	    return (AssetStore[_uuid].name, AssetStore[_uuid].description, AssetStore[_uuid].manufacturer);
	}
	function transferAsset(address _to, string memory _uuid) public {
	//	if(!AssetStore[_uuid].initialized) {
    //	emit RejectTransfer(msg.sender, _to, _uuid, "No asset with this UUID exists");
	//	return;
	//  } 
	require((AssetStore[_uuid].initialized),"No asset with this UUID exists");

    //  if(!WalletStore[msg.sender][_uuid]) {
    //	emit RejectTransfer(msg.sender, _to, _uuid, "Sender does not own this asset.");
	//  	return;
	//}
	require((WalletStore[msg.sender][_uuid]),"Sender does not own this asset.");

    WalletStore[msg.sender][_uuid] = false;
	WalletStore[_to][_uuid] = true;
	addressArray.push(_to);
	ownerCount++;
	emit AssetTransfer(msg.sender, _to,	_uuid);
    }
	function displayOwner()public view  returns (address[] memory owner) { //iterate over the loop to display history of owners
	    //for(uint i=0; i< ownerCount; i++) {
	    owner = addressArray;
	    //return owner;
	    //}
	}
}
