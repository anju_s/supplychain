const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Define collection and schema for Business
let Employee = new Schema({
  emp_id: {
    type: Number
  },
  emp_name: {
    type: String
  },
  emp_address: {
    type: String
  },
  emp_department: {
    type: String
  }
},{
    collection: 'employee'
});

module.exports = mongoose.model('Employee', Employee);