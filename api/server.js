const express = require('express'),
    path = require('path'),
    bodyParser = require('body-parser'),
    cookieParser = require('cookie-parser'),
    logger = require('morgan'),
    cors = require('cors'),
    mongoose = require('mongoose'),
    config = require('./DB');
    var Web3 = require('web3');


var contractJSON=require(path.join(__dirname,'../build/contracts/AssetTracker.json'));
contractAddress=contractJSON.networks['4002'].address;
abi=contractJSON.abi;

web3=new Web3(Web3.providers.HttpProvider('http://localhost:8545'));
MyContract=new web3.eth.Contract(abi,contractAddress);

const productRoutes = require('./routes/product.route');
/*mongoose.Promise = global.Promise;
mongoose.connect(config.DB, { useNewUrlParser: true }).then(
  () => {console.log('Database is connected') },
  err => { console.log('Can not connect to the database'+ err)}
);*/

const app = express();
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(bodyParser.json());
app.use(cors());
app.use('/product', productRoutes);
const port = process.env.PORT || 4000;

const server = app.listen(port, function(){
  console.log('Listening on port ' + port);
});